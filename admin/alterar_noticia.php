<form action="op_noticia.php?update=1" method="POST" enctype="multipart/form-data">
    <fieldset>
        <legend>Alterando noticia</legend>
        <div>
            <input type="hidden" name="id_noticia" value="<?php echo $_GET['id_noticia'];?>">
        </div>
        <div>
            <label>
                Id Categoria <br>
                <select name="id_categoria" id="id_categoria">                
                    <?php                    
                        require_once('../config.php');
                        $cats = Categoria::listar();                        
                        foreach ($cats as $cat) 
                        {
                            if($cat['categoria']==$_GET['nome_categoria'])
                            {
                                echo "<option value=".$cat['id_categoria']." selected>".$cat['categoria']."</option>";
                            }
                            else
                            {
                                echo "<option value=".$cat['id_categoria'].">".$cat['categoria']."</option>";
                            }                                                        
                        }
                    ?>
                </select>
            </label>
        </div>
        <div>
            <label for="">Titulo</label><br>
            <input type="text" name="titulo" value="<?php echo $_GET['titulo']?>">
            <br>
        </div>
        <div>
            <label for="">Img</label><br>
            <input type="file" name="img_noticia" value="">
            <img src="foto/<?php echo $_GET['img'];?>" alt="" width="100" height="100">
            <input type="hidden" id="img_atual" name="img_atual" value="<?php echo $_GET['img'];?>">
            <br>
        </div>
        <div>
            <label for="">Visitas</label><br>
            <input type="text" name="visita" value="<?php echo $_GET['visita']?>">
            <br>
        </div>
        <div>
            <label for="">Data da noticia</label><br>
            <input type="text" name="data" value="<?php echo $_GET['data']?>">
            <br>
        </div>
        <div>
            <label>Ativo</label><br>
            <input type="checkbox" name="ativo" <?php echo $_GET['ativo']=='s'?'checked':''?>>
            <br>            
        </div>
        <div>
            <label for="">noticia</label><br>
            <input type="text" name="noticia" value="<?php echo $_GET['noticia']?>">
        </div>
        <br>
        <br>
        <input type="submit" id="" name="btn_update_noticia" value="Atualizar Noticia">
    </fieldset>
</form>