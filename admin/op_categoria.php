<?php
    require_once('../config.php');

    // Inserção       
    $cat = new Categoria();
    if($_GET['inserir']==1)   
    {
        if(count($cat->inserir($_POST['txt_categoria'],
        isset($_POST['check_ativo'])?'1':'0'))>0)
        {
            header('location:principal.php?link=2&msg=ok');
        }
        else
        {
            header('location:principal.php?link=2&msg=erro');
        }
    } 
    // Remover   
    if($_GET['excluir'] == 1 && isset($_GET['id']))
    {
        $cat = new Categoria();
        $cat->delete($_GET['id']);
        header('location:principal.php?link=3&msg=ok');
    }
    else
    {
        header('location:principal.php?link=3&msg=erro');
    }    
    // Alterar  
    if($_GET['update']==1) 
    {   
        if(isset($_POST['id_cat']))     
        {
            $cat = new Categoria();
            $cat->update($_POST['id_cat'],$_POST['categoria'],
            isset($_POST['ativo'])? 1:0);
            header('location:principal.php?link=3&msg=ok');
        }
        else
        {
            header('location:principal.php?link=3&msg=erro');
        }
    }
    
?>