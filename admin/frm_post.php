<div id="box-cadastro">
    <div id="formulario-menor">
        <form action="op_post.php?inserir=1" method="post">
            <fieldset>
                <legend>Inserir Post</legend>
                <?php 
                    require_once('../config.php');
                    $cats = Categoria::listar();
                ?>
                <label for="">
                    <span>Categoria</span>
                    <select name="id_categoria_post" id="id_categoria_post" style="width: 100%; height: 30px; font-size: 15pt">            
                        <?php 
                            foreach ($cats as $cat) 
                            {
                                echo "<option value=".$cat['id_categoria'].">".$cat['categoria']."</option>";
                            }
                        ?>            
                    </select>
                </label>
                <label for="">
                    <span>Titulo</span>
                    <input type="text" name="titulo_post">
                </label>
                <label for="">
                    <span>Descricao</span>
                    <input type="text" name="descricao_post">
                </label>
                <label for="">
                    <span>img</span>
                    <input type="file" name="img_post">
                </label>                
                <label for="">
                    <span>Data</span>
                    <input type="date" name="data_post">
                </label>
                <label for="">
                    <span>Ativo</span>
                    <input type="checkbox" name="ativo_post" checked>
                </label>
                <label for="">
                    <input type="submit" value="Cadastrar" name="btn_inserir_post">
                </label>
            </fieldset>
        </form>
    </div>
</div>