<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">    
    <title>Alteração de Post</title>
</head>
<body>    
    <form action="op_post.php?update=1" method="POST" enctype="multipart/form-data">
        <fieldset>
            <legend>Alteração do Post</legend>
            <div>
                <input type="hidden" name="id" value="<?php echo filter_input(INPUT_GET,'id_post');?>">
            </div>
            <div>
                <label for="">Categoria</label>
                <select name="categoria" id="categoria">
                    <?php
                        require_once('../config.php');
                        foreach(Categoria::listar() as $categoria)
                        {
                            if(filter_input(INPUT_GET,'categoria') == $categoria['categoria'])
                            {
                                echo "<option value=".$categoria['id_categoria']." selected>".$categoria['categoria']."</option>";
                            }
                            else
                            {
                                echo "<option value=".$categoria['id_categoria'].">".$categoria['categoria']."</option>";
                            }
                        }                        
                    ?>
                </select>
            </div>
            <div>
                <label for="">Titulo</label>
                <input type="text" name="titulo" value="<?php echo filter_input(INPUT_GET,'titulo');?>">
            </div>                       
            <div>
                <label for="">Descricao</label>
                <input type="text" name="descricao" value="<?php echo filter_input(INPUT_GET,'descricao');?>">
            </div>
            <div>
                <label for="">Img</label>
                <input type="text" name="img" value="<?php echo filter_input(INPUT_GET,'img');?>">
            </div>
            <div>
                <label for="">Visitas</label>
                <input type="text" name="visitas" value="<?php echo filter_input(INPUT_GET,'visitas');?>">
            </div>
            <div>
                <label for="">Data</label>
                <input type="date" name="data" value="<?php echo filter_input(INPUT_GET,'data');?>">
            </div>
            <div>
                <label for="">Ativo</label>
                <input type="checkbox" name="ativo" <?php echo filter_input(INPUT_GET,'ativo')=='s'?'checked':'' ;?>>
            </div>
            <div>                
                <input type="submit" name="btn_alterar_post" value="Alterar Post">
            </div>
        </fieldset>
    </form>    
</body>
</html>