<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">    
    <title>Alteração Banner</title>
</head>
<body>
    <form action="op_banner.php?update=1" method="post">    
        <fieldset>
            <legend>Alterar Banner</legend>
                <div>
                    <input type="hidden" name="id" value="<?php echo filter_input(INPUT_GET,'id_banner');?>">
                </div>
                <div>
                    <label for="">Titulo</label>
                    <input type="text" name="titulo" value="<?php echo filter_input(INPUT_GET,'titulo');?>">
                </div>
                <div>
                    <label for="">Link</label>
                    <input type="text" name="link" value="<?php echo filter_input(INPUT_GET,'link');?>">
                </div>
                <div>
                    <label for="">Img</label>
                    <input type="text" name="img" value="<?php echo filter_input(INPUT_GET,'img');?>">
                </div>
                <div>
                    <label for="">alt</label>
                    <input type="text" name="alt" value="<?php echo filter_input(INPUT_GET,'alt');?>">
                </div>            
                <div>
                    <label for="">Ativo</label>
                    <input type="checkbox" name="ativo" <?php echo filter_input(INPUT_GET,'ativo')=='s'?'checked':'';?>>
                </div>
                <div>
                    <input type="submit" name="btn_alterar_banner" value="Alterar Banner">
                </div>            
        </fieldset>
    </form>
</body>
</html>