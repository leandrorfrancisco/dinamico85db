<?php      
    // aqui vai a declaração do var banner e o if
    require_once('../config.php');  
    $banner = new Banner();    
    if(count($banners_retornados = $banner->listarBanner()) > 0 )
    {    
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista do Banner</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_banner" width='100%' border="0" cellpadding="0" cellspacing="1" bgcolor="">
        <tr bgcolor="#993300" align="center">
            <th width="10%" height="2" align="rigth"><font size="2" color="#fff">ID</font></th>
            <th width="15%" height="2" align="center"><font size="2" color="#fff">Titulo</font></th>
            <th width="20%" height="2" align="center"><font size="2" color="#fff">Link</font></th>
            <th width="15%" height="2" align="center"><font size="2" color="#fff">Caminho</font></th>
            <th width="20%" height="2" align="center"><font size="2" color="#fff">Alt</font></th>
            <th width="7%" height="2" align="center"><font size="2" color="#fff">Ativo</font></th>
            <th colspan="2" align="center"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
            foreach($banners_retornados as $banner)
            {
                
        ?>
        <tr>
            <td><?php echo $banner['id_banner'] ?></td>
            <td><?php echo $banner['titulo_banner'] ?></td>
            <td><?php echo $banner['link_banner'];?></td>
            <td><?php echo $banner['img_banner'];?></td>
            <td><?php echo $banner['alt']; ?></td>
            <td><?php echo $banner['banner_ativo']=='s'?'Sim':'Não'; ?></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000"></font><a href="<?php echo 'alterar_banner.php?id_banner='.$banner['id_banner'].
            '&titulo='.$banner['titulo_banner'].'&link='.$banner['link_banner'].'&img='.$banner['img_banner'].'&alt='.$banner['alt'].'&ativo='.$banner['banner_ativo'];?>">Alterar</a></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000"></font><a href="<?php echo 'op_banner.php?delete=1&id='.$banner['id_banner'];?>">Excluir</a></td>
        </tr>
        <?php
            }
        }
        ?>
    </table>
</body>
</html>