<?php
require_once('../config.php');
    $not = new Noticia();
    #Inserindo noticia
    if(isset($_POST['btn_inserir_noticia']))
    {
        var_dump($_FILES['img_noticia']);
        $imagem = upload_imagem($_FILES['img_noticia']);        
        $not->inserirNoticia($_POST['id_categoria'],$_POST['titulo_noticia'],$imagem[0],$_POST['data_noticia'],isset($_POST['ativo_noticia'])?'s':'n',$_POST['text_noticia']);
        if($not->getId()>0)
        {
            header('location:principal.php?link=6&msg=ok');
        }
        else
        {
            header('location:principal.php?link=6&msg=erro');
        }
    }
    #Excluindo noticia
    if(isset($_GET['excluir']))
    {
        if($_GET['id_noticia']>0)
        {
            $not->deleteNoticia($_GET['id_noticia']);
            header('location:principal.php?link=7');
        }
    }
    #Atualizando noticia
    if(isset($_GET['update']))
    {        
        if(isset($_POST['btn_update_noticia']) && $_GET['update']==1)
        {
            $foto = $_FILES['img_noticia'];
            if(isset($_FILES['img_noticia']) && !empty($foto['name']))
            { 
                $imagem = upload_imagem();
                $not->updateNoticia($_POST['id_noticia'],$_POST['id_categoria'],$_POST['titulo'],$imagem[0],$_POST['data'],isset($_POST['ativo'])?'s':'n',$_POST['noticia']);
                header('location:principal.php?link=7');                
            }
            else if(isset($_POST['img_atual']))
            {                
                $not->updateNoticia($_POST['id_noticia'],$_POST['id_categoria'],$_POST['titulo'],$_POST['img_atual'],$_POST['data'],isset($_POST['ativo'])?'s':'n',$_POST['noticia']);
                header('location:principal.php?link=7');
            }           
        }
        else
        {
            header('location:principal.php?link=7&erro=1');
        }
    }    
?>