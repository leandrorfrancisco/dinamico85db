<?php

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Formulário Usuário</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div id="formulario-menor">
        <form action="op_usuario.php" method="post">
            <fieldset>
                <input type="hidden" id="id" name="id">
                <label for="">Nome</label>
                <input type="text" name="txt_nome" required>
                <p>
                <label for="">Email</label>
                <input type="text" name="txt_email" required>
                <p>
                <label for="">Senha</label>
                <input type="password" name="txt_senha" required>
                <p>
                <label for="">Confirma Senha</label>
                <input type="password" name="confirma_senha" required>
            
                  <br>
                <input type="submit" name="cadastro_usuario" value="Cadastrar Usuário" class="botao">

            </fieldset>
        </form>
    </div>
</body>
</html>
