<?php
class Usuario{
    private $id;
    private $nome;
    private $email;
    private $senha;

     // declaração de métodos de acesso

    public function getId(){
        return $this->id;
    }
    public function setId($value){
        $this->id = $value;
    }
    public function getNome(){
        return $this->nome;
    }
    public function setNome($value){
        $this->nome = $value;
    }

    public function getEmail(){
        return $this->email;
    }
    public function setEmail($value){
        $this->foto = $value;
    }

    public function getSenha(){
        return $this->senha;
    }
    public function setSenha($value){
        $this->senha = $value;
    }
    
    public function loadById($_id){
        $sql = new Sql();
        $results = $sql->select("SELECT * FROM usuario WHERE id = :id",array(':id'=>$_id));
        if (count($results)>0) {
            $this->setData($results[0]);
        }
    }
    public static function getList(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM usuario order by nome");
    }
    public static function search($nome_user){
        $sql = new Sql();
        return $sql->select("SELECT * FROM usuario WHERE nome LIKE :nome",
            array(":nome"=>"%".$nome_user."%"));
    }

    //efetuar login 

    public function efetuarLogin($_email, $_senha){
        $sql = new Sql();
        $senha_cript = md5($_senha);
        $results = $sql->select("SELECT * FROM usuario WHERE email = :email AND senha = :senha",
        array(':email'=>$_email,":senha"=>$senha_cript));
        if (count($results)>0) {
            $this->setData($results[0]);
        }


    }
    public function setData($dados){ 
        $this->setId($dados['id']);
        $this->setNome($dados['nome']);
        $this->setEmail($dados['email']);
        $this->setSenha($dados['senha']);




    }
    public function insert(){
        $sql = new Sql();
        $results = $sql->select("CALL sp_usuario_insert(:nome, :email, :senha)",
            array(
                ":nome"=>$this->getNome(),
                ":email"=>$this->getEmail(),
                ":senha"=>$this->getSenha(),

            ));
        if (count($results)>0) {
            $this->setData($results[0]);
        }
    }
    public function update($_id, $_nome, $_email, $_senha){
        $sql = new Sql();
        $sql->query("UPDATE usuario SET nome = :nome, email = :email, senha = :senha  
            WHERE id = :id",
            array(
                ":id"=>$_id,
                ":nome"=> $_nome,
                ":email"=>$_email,
                ":senha"=>$_senha,
            ));
    }
    public function delete(){
        $sql = new Sql();
        $sql->query("DELETE FROM usuario WHERE id = :id",array(":id"=>$this->getId()));
    }
    public function __construct($_nome="",$_email="",$_senha=""){
        $this->nome = $_nome;
        $this->email = $_email;
        $this->senha = $_senha;


    }

}

?>