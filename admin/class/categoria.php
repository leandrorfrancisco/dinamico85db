<?php
    // class Categoria{ 
    //     // Atributos
    //     private $id_categoria;
    //     private $categoria;
    //     private $cat_ativo;
        
    //     // Métodos de acesso (Getters and Setters)
    //     public function getId(){
    //     return $this->id_categoria;
    //     }
    //     public function setId($value){
    //         $this->id_categoria = $value;
    //     }
    //     public function getCategoria(){
    //         return $this->categoria;
    //     }
    //     public function setCategoria($value){
    //         $this->categoria = $value;
    //     }
    //     public function getAtivo(){
    //         return $this->cat_ativo;
    //     }
    //     public function setAtivo($value){
    //         $this->cat_ativo = $value;
    //     }

    //     public function loadById($_id){
    //         $sql = new Sql();
    //         $results = $sql->select('SELECT * FROM ctegoria WHERE id_categoria = :id', array(':id'=>$_id));
    //         if (count($results)>0) {
    //             $this->setData($results[0]);
    //         }
    //     }
    //     public static function getList(){
    //         $sql = new Sql();
    //         return $sql->select('SELECT * FROM categoria order by categoria');
    //     }
    //     public static function search($categoria){
    //         $sql = new Sql();
    //         return $sql->select('SELECT * FROM categoria WHERE categoria LIKE :categoria', array(':categoria'=>'%'.$categoria.'%'));
    //     }
    //     public function setData($data){
    //         $this->setId($data['id_categora']);
    //         $this->setCategoria($data['categoria']);
    //         $this->setAtivo($data['cat_ativo']);
    //     }
    //     public function insert(){
    //         $sql = new Sql();
    //         $results = $sql->select('CALL sp_categoria_insert(:categoria,:ativo', array(
    //             ':categoria'=>$this->getCategoria(),
    //             ':ativo'=>$this->getAtivo()
    //         ));
    //         if (count($results)>0) {
    //             $this->setData($results[0]);
    //         }
    //     }
    //     public function update($_id, $_categoria, $_ativo){
    //         $sql = new Sql();
    //         $sql->query('UPDATE categoria SET categoria=:categoria, ativo=:ativo WHERE id_categoria=:id', array(
    //             ':id'=>$_id,
    //             ':categoria'=>$_categoria,
    //             ':ativo'=>$_ativo
    //         ));
    //     }
    //     public function delete(){
    //         $sql = new Sql();
    //         $sql->query('DELETE FROM categoria WHERE id_categoria=:id', array(':id'=>$this->getId()));
    //     }
    //     public function __construct($_categoria='', $_ativo=''){
    //         $this->categoria = $_categoria;
    //         $this->cat_ativo = $_ativo;
    //     }
    //     public static function listar()
    //     {
    //         $sql = new Sql();
    //         return $sql->select('SELECT * FROM categoria');
    //     }
    // }
?>

<?php

    class Categoria
    {

        #Definindo Atributos
        private $id;
        private $categoria;
        private $cat_ativo;
        #Métodos para acessar atributos
        public function getId()
        {
            return $this->id;
        }
        public function setId($value)
        {
            $this->id=$value;
        }
        public function getCategoria()
        {
            return $this->id;
        }
        public function setCategoria($value)
        {
            $this->categoria = $value;
        }
        public function getCatAtivo()
        {
            return $this->cat_ativo;
        }
        public function setCatAtivo($value)
        {
            $this->cat_ativo = $value;
        }
        #Inserindo categoria no banco de dados
        public function inserir($_categoria,$_ativo)
        {
            $sql = new Sql();
            return $sql->select('call sp_categoria_insert(:categoria,:cat)',array(':categoria'=>$_categoria,':cat'=>$_ativo));                
        }
        #Listando todas as categoria do banco de dados
        public static function listar()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM categoria');
        }
        #Consultando categoria pelo id
        public function consultarId($_id)
        {
            $sql = new Sql();
            $sql->select('select * from categoria where id_categoria = :id',array(':id'=>$_id));
        }
        #Consultado categoria pelo nome
        public function searchByName($_categoria)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM categoria where categoria LIKE :categoria',array(':categoria'=>'%'.$_categoria.'%'));
        }
        #Atualiza se o nome e se a categoria está ativa ou não
        public function update($_id,$_categoria,$_cat_ativo)
        {
            $sql = new Sql();
            $sql->query('UPDATE categoria set categoria = :categoria, cat_ativo = :ativo where id_categoria = :id',
            array(':id'=>$_id,':categoria'=>$_categoria,':ativo'=>$_cat_ativo));
        }
        #Deleta a categoria do banco de dados
        public function delete($_id)
        {
            $sql = new Sql();
            $sql->query('delete from categoria where id_categoria = :id',array(':id'=>$_id));
        }
        #Define o valor que o banco de dados retornar para os atributos
        public function setData($data)
        {
            $this->setId($data['id_categoria']);
            $this->setCategoria($data['categoria']);
            $this->setCatAtivo($data['cat_ativo']);
        }
        public function __construct($_id='',$_categoria='',$_ativo='')
        {
            $this->id = $_id;
            $this->categoria = $_categoria;
            $this->cat_ativo = $_ativo;
        }
    }
?>