<?php

    class Banner
    {
        private $id_banner;
        private $titulo_banner;
        private $link_banner;
        private $img_banner;
        private $alt;
        private $ativ_banner;
        
        // ID
        public function getId()
        {
            return $this->id_banner;
        }
        public function setId($value)
        {
            $this->id_banner = $value;
        }
        // Titulo
        public function getTitulo()
        {
            return $this->titulo_banner;
        }
        public function setTitulo($value)
        {
            $this->titulo_banner = $value;
        }
        // Link
        public function getLink()
        {
            return $this->link_banner;
        }
        public function setLink($value)
        {
            $this->link_banner = $value;
        }
        // img
        public function getImg()
        {
            return $this->img_banner;
        }
        public function setImg($value)
        {
            $this->img_banner = $value;
        }
        // Alt
        public function getAlt()
        {
            return $this->alt;
        }
        public function setAlt($value)
        {
            $this->alt = $value;
        }
        // Ativo
        public function getAtivo()
        {
            return $this->ativ_banner;
        }
        public function setAtivo($value)
        {
            $this->ativ_banner = $value;
        }


        public function pesquisaId($_id)
        {
            $sql = new Sql();
            return $sql->select('select * from banner where id_banner = :id',array(':id'=>$_id));
        }

        public function insertBanner($_titulo,$_link,$_img,$_alt,$_ativo)
        {
            $sql = new Sql();
            $results = $sql->select('CALL sp_banner_insert(:titulo_banner,:link_banner,:img_banner,:alt,:banner_ativo)',
            array(
                ':titulo_banner'=>$_titulo,
                ':link_banner'=>$_link,
                ':img_banner'=>$_img,
                ':alt'=>$_alt,
                ':banner_ativo'=>$_ativo));

            if (count($results) > 0) 
            {
                $this->setData($results[0]);
            }              
        }

        public function deleteBanner($_id)
        {
            $sql = new Sql();
            $sql->query('DELETE from banner WHERE id_banner = :id',array(':id'=>$_id));
        }

        public function updateBanner($_id,$_titulo,$_link,$_img,$_alt,$_ativo)
        {
            $sql = new Sql();
            $sql->query('UPDATE banner set titulo_banner = :titulo,link_banner = :link, img_banner = :img,alt=:alt,banner_ativo=:ativo where id_banner = :id',
            array(
                ':titulo_banner'=>$_titulo,
                ':link_banner'=>$_link,
                ':img_banner'=>$_img,
                ':alt'=>$_alt,
                ':banner_ativo'=>$_ativo,
                ':id'=>$_id));
        }

        public function listarBanner()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM banner');
        }

        public function pesquisaTitulo($_titulo)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM banner where titulo_banner LIKE :titulo',array(':titulo'=>'%'.$_titulo.'%'));
        }

        public function __construct($_id='',$_titulo='',$_link='',$_img='',$_alt='',$_ativo='')
        {
            $this->id_banner = $_id;
            $this->titulo_banner = $_titulo;
            $this->link_bannernk = $_link;
            $this->img_banner = $_img;
            $this->alt = $_alt;
            $this->ativ_banner = $_ativo;
        }

        public function setData($data)
        {
            $this->setId($data['id_banner']);
            $this->setTitulo($data['titulo_banner']);
            $this->setLink($data['link_banner']);
            $this->setImg($data['img_banner']);
            $this->setAlt($data['alt']);
            $this->setAtivo($data['banner_ativo']);
        }
    }
?>