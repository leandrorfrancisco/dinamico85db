<?php
    require_once('../config.php');
    #Inserindo POST    
    if(isset($_POST['btn_inserir_post']))
    {
        $post = new Post();
        $post->inserirPost($_POST['id_categoria_post'],$_POST['titulo_post'],$_POST['descricao_post'],$_POST['img_post'],$_POST['data_post'],isset($_POST['ativo_post'])?'s':'n');
        if($post->getId()>0)
        {
            header('location:principal.php?link=4&msg=inserido');
        }
        else
        {
            header('location:principal.php?link=4&msg=erro');
        }
    }    
    #delete post
    if($_GET['excluir']==1 && isset($_GET['id_post']))
    {        
        $post = new Post();
        $post->deletePost($_GET['id_post']);
        header('location:principal.php?link=5&msg=excluido');        
        $_GET['excluir'] = null;
        $_GET['id_post'] = null;
    }
    #Update post
    if($_GET['update']==1 && isset($_POST['id']) && isset($_POST['btn_alterar_post']))
    {
        $post = new Post();
        $post->updatePost($_POST['id'],$_POST['categoria'],$_POST['titulo'],$_POST['descricao'],$_POST['img'],$_POST['visitas'],$_POST['data'],isset($_POST['ativo'])?'s':'n');
        header('location:principal.php?link=5&msg=alterado');
    }
?>