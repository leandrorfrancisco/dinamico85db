<?php  
    require_once('../config.php');
    //Inserindo Banner


     if(isset($_POST['btn_cadastrar']))
    {         
        $ban = new Banner();
        $ban->insertBanner($_POST['txt_titulo'],$_POST['txt_link'],$_POST['txt_caminho'],$_POST['txt_alt'],isset($_POST['check_ativo'])? '1':'0');
        if($ban->getId()>0)        
        {
            header('location:principal.php?link=8&msg=ok');
        }
        else
        {
            header('location:principal.php?link=8');
        }        
    }
    // Deletar banner

    if($_GET['delete']==1 && isset($_GET['id']))
    {
        $ban = new Banner();
        $ban->deleteBanner($_GET['id']);        
        header('location:principal.php?link=9');
    }
    // Atualizar Banner
    if($_GET['update']==1 && isset($_POST['id']))
    {
        $ban = new Banner();
        $ban->updateBanner($_POST['id'],$_POST['titulo'],$_POST['link'],$_POST['img'],$_POST['alt'],isset($_POST['ativo'])?'s':'n');
        header('location:principal.php?link=9');
    }
?>