<?php

require_once('../config.php');


if(isset($_POST['cadastro']))
{
    $adm = new Administrador
    (
        $_POST['nome'],
        $_POST['email'],
        $_POST['login'],
        $_POST['senha']
    );
    $adm->insert();
    if($adm->getId()!=null)
    {
        header('location:principal.php?link=11&msg=ok');
        var_dump($adm);
    }
}

// excluir/deletar Administrador
$id = filter_input(INPUT_GET,'id');
$excluir = filter_input(INPUT_GET,'excluir');
if(isset($id) && $excluir==1){
    $admin = new Administrador();
    $admin->setId($id);
    $admin->delete();
    header('location:principal.php?link=11&msg=ok');
}
//Alterar o Administrador
if (isset($_POST['alterar'])){
    $adm = new Administrador();
    $adm->update($_POST['id'],$_POST['nome'],$_POST['email'],$_POST['login']);
    header('location:principal.php?link=11&msg=ok');    
}

?>