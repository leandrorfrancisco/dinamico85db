<?php
    require_once('../config.php');            
    $post = new Post();

    if(count($posts = $post->listarPostInner()) > 0)
    {
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Lista Noticias</title>
</head>
<body>
    <table width='100%' border="" cellpadding="0" cellspacing="1" bgcolor="">
        <tr bgcolor="#993300" align="center">
            <th width="5%" height="2" align="rigth"><font size="2" color="#fff">ID</font></th>
            <th width="5%" height="2" align="rigth"><font size="2" color="#fff">Categoria</font></th>
            <th width="20%" height="2" align="rigth"><font size="2" color="#fff">Titulo</font></th>
            <th width="20%" height="2" align="rigth"><font size="2" color="#fff">Descricao</font></th>
            <th width="10%" height="2" align="rigth"><font size="2" color="#fff">Img</font></th>
            <th width="5%" height="2" align="rigth"><font size="2" color="#fff">Visitas</font></th>
            <th width="10%" height="2" align="rigth"><font size="2" color="#fff">Data</font></th>
            <th width="5%" height="2" align="rigth"><font size="2" color="#fff">Ativo</font></th>
            <th colspan="2" align="center"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php            
            foreach($posts as $pot)
            {
        ?>
        <tr align="center">
            <td><?php echo $pot['id_post']?></td>
            <td><?php echo $pot['categoria']?></td>
            <td><?php echo $pot['titulo_post']?></td>
            <td><?php echo $pot['descricao_post']?></td>
            <td><?php echo $pot['img_post']?></td>
            <td><?php echo $pot['visitas']?></td>
            <td><?php echo $pot['data_post']?></td>
            <td><?php echo $pot['post_ativo']?></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000">
                <a href="<?php echo 'alterar_post.php?id_post='.$pot['id_post'].'&categoria='.$pot['categoria'].'&titulo='.$pot['titulo_post'].
                '&descricao='.$pot['descricao_post'].'&img='.$pot['img_post'].'&visitas='.$pot['visitas'].'&data='.$pot['data_post'].'&ativo='.$pot['post_ativo']?>">Alterar</a>
            </font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000">
                <a href="<?php echo 'op_post.php?excluir=1&id_post='.$pot['id_post']?>">Excluir</a>
            </font></td>
        </tr>
        <?php
            }
        }
        ?>
    </table>
</body>
</html>