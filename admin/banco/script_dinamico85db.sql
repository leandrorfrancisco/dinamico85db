create database dinamico85db;

use dinamico85db;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Estrutura da tabela `administrador`
--

CREATE TABLE `administrador` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `login` varchar(100) NOT NULL,
  `senha` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `administrador`
--

INSERT INTO `administrador` (`id`, `nome`, `email`, `login`, `senha`) VALUES
(1, 'Davi Barros', 'davi@ti85.senac', 'Araçá', '123456');

-- --------------------------------------------------------

--
-- Estrutura da tabela `banner`
--

CREATE TABLE `banner` (
  `id_banner` int(11) NOT NULL,
  `titulo_banner` varchar(255) NOT NULL,
  `link_banner` varchar(255) NOT NULL,
  `img_banner` varchar(150) NOT NULL,
  `alt` varchar(255) NOT NULL,
  `banner_ativo` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `categoria` varchar(150) NOT NULL,
  `cat_ativo` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--
create table if not exists usuario(
id int not null auto_increment,
nome varchar(50) not null,
email varchar(50) not null,
foto varchar(100) not null,
primary key (id)
);




CREATE TABLE `noticias` (
  `id_noticia` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `titulo_noticia` varchar(255) NOT NULL,
  `img_noticia` varchar(100) NOT NULL,
  `visita_noticia` int(11) NOT NULL,
  `data_noticia` date NOT NULL,
  `noticia_ativo` varchar(1) NOT NULL,
  `noticia` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `post`
--

CREATE TABLE `post` (
  `id_post` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `titulo_post` varchar(250) NOT NULL,
  `descricao_post` text NOT NULL,
  `img_post` varchar(200) NOT NULL,
  `visitas` int(11) NOT NULL,
  `data_post` date NOT NULL,
  `post_ativo` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id_noticia`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id_post`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id_noticia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

 -----------------------------------------------------------------------------Criando Procedures--------------------------------------------------------------------------------------

delimiter $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_adm_insert`(
_nome varchar(200),
_email varchar(200),
_login varchar(100),
_senha varchar(100)
)
BEGIN
insert into administrador (nome, email, login, senha)
values (_nome, _email, _login, _senha);
select * from administrador where id = (select @@identity);
END;

delimiter $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_categoria_insert`(
_categoria varchar(150),
_ativ varchar(1)
)
BEGIN
insert into categoria (categoria, cat_ativo)
values  (_categoria, _ativ);
select * from categoria where id_categoria = (select @@identity);
END;

call sp_categoria_insert('test', '1');

delimiter $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_banner_insert`(
_titulo_banner varchar(255),
_link_banner varchar(225),
_img_banner varchar(150),
_alt varchar(255),
_banner_ativo varchar(1)
)
BEGIN
insert into banner (titulo_banner, link_banner, img_banner, alt, banner_ativo)
values  (titulo_banner, link_banner, img_banner, alt, banner_ativo);
select * from banner where id = (select @@identity);
END

delimiter $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_post_insert`(
_titulo_post varchar(250),
_descricao_post text,
_img_post varchar(200),
_visitas int (11),
_data_post date,
_post_ativo varchar(1)
)
BEGIN
insert into post (titulo_post, descricao_post, img_post, visitas, data_post, post_ativo)
values   (titulo_post, descricao_post, img_post, visitas, data_post, post_ativo);
select * from post where id = (select @@identity);
END

delimiter $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_noticias_insert`(
_titulo_post varchar(255),
_img_post varchar(100),
_visita_noticia int (11),
_data_noticia date,
_noticia_ativo varchar(1),
_noticia text
)
BEGIN
insert into noticia (titulo_noticia, img_noticia, visita_noticia, data_noticia, noticia_ativo, noticia)
values    (titulo_noticia, img_noticia, visita_noticia, data_noticia, noticia_ativo, noticia);
select * from noticias where id = (select @@identity);
END
